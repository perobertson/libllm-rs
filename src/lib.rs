pub mod errors;
pub mod openai;

pub use openai_dive::v1::api::Client;
pub use openai_dive::v1::resources::assistant::{assistant::Assistant, message::Message};
