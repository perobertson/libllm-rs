use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("command failed error: {msg}")]
    APIError { msg: String },
}
