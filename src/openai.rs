use openai_dive::v1::{
    models::Gpt4Engine,
    resources::{
        assistant::{
            assistant::AssistantParameters,
            message::{CreateMessageParameters, MessageRole},
            run::{CreateRunParameters, Run},
            thread::{CreateThreadParameters, Thread},
        },
        shared::{DeletedObject, ListParameters},
    },
};
use tracing::{error, trace};

use crate::{Assistant, Client, Message, errors::Error};

/// Create a new assistant
///
/// See: <https://platform.openai.com/docs/api-reference/assistants/createAssistant>
pub async fn assistant_create(
    client: &Client,
    parameters: Option<AssistantParameters>,
) -> Result<Assistant, Error> {
    let parameters = if let Some(parameters) = parameters {
        parameters
    } else {
        AssistantParameters {
            model: Gpt4Engine::Gpt4OMini.to_string(), // Free plan
            name: Some("Default".to_string()),
            instructions: Some("be brief".to_string()), // Instructions are needed
            tools: Some(vec![]),                        // None is invalid
            ..Default::default()
        }
    };
    let assistant = client
        .assistants()
        .create(parameters)
        .await
        .map_err(|err| {
            let msg = format!("failed to create assistant: {}", err);
            error!("{}", msg);
            Error::APIError { msg }
        })?;
    trace!("{:#?}", assistant);
    Ok(assistant)
}

/// Delete an assistant
///
/// See: <https://platform.openai.com/docs/api-reference/assistants/deleteAssistant>
pub async fn assistant_delete(client: &Client, assistant_id: &str) -> Result<DeletedObject, Error> {
    let result = client
        .assistants()
        .delete(assistant_id)
        .await
        .map_err(|err| {
            let msg = format!("failed to delete assistant: {}", err);
            error!(msg);
            Error::APIError { msg }
        })?;
    trace!("{:#?}", result);
    Ok(result)
}

/// List available assistants
///
/// See: <https://platform.openai.com/docs/api-reference/assistants/listAssistants>
pub async fn assistant_list(
    client: &Client,
    parameters: Option<ListParameters>,
) -> Result<Vec<Assistant>, Error> {
    let mut assistants: Vec<Assistant> = vec![];
    let mut parameters = if let Some(parameters) = parameters {
        parameters
    } else {
        ListParameters {
            after: None,
            before: None,
            limit: Some(100),
            order: Some("asc".to_string()), // puts the most recent at the end
        }
    };
    loop {
        let mut response = client
            .assistants()
            .list(Some(parameters.to_owned()))
            .await
            .map_err(|err| {
                let msg = format!("failed to list assistants: {}", err);
                error!(msg);
                Error::APIError { msg }
            })?;
        trace!("{:#?}", response);
        assistants.append(&mut response.data);
        if !response.has_more {
            break;
        } else if let Some(assistant) = assistants.last() {
            parameters.after = Some(assistant.id.clone());
        } else {
            // This should be unreachable because there should have been data
            // in the response and that was appended to the array.
            break;
        }
    }
    Ok(assistants)
}

/// Create a message on a thread
///
/// See: <https://platform.openai.com/docs/api-reference/messages/createMessage>
pub async fn message_create(
    client: &Client,
    thread_id: &str,
    content: String,
) -> Result<Message, Error> {
    let parameters = CreateMessageParameters {
        role: MessageRole::User,
        content,
        ..Default::default()
    };
    let message = client
        .assistants()
        .messages()
        .create(thread_id, parameters)
        .await
        .map_err(|err| {
            let msg = format!("failed to create message: {}", err);
            error!("{}", msg);
            Error::APIError { msg }
        })?;
    trace!("{:#?}", message);
    Ok(message)
}

/// List messages on a thread
///
/// See: https://platform.openai.com/docs/api-reference/messages/listMessages
pub async fn message_list(
    client: &Client,
    thread_id: &str,
    parameters: Option<ListParameters>,
) -> Result<Vec<Message>, Error> {
    let mut messages: Vec<Message> = vec![];
    let mut parameters = if let Some(parameters) = parameters {
        parameters
    } else {
        ListParameters {
            after: None,
            before: None,
            limit: Some(100),
            order: Some("asc".to_string()), // puts the most recent at the end
        }
    };
    loop {
        let mut response = client
            .assistants()
            .messages()
            .list(thread_id, Some(parameters.to_owned()))
            .await
            .map_err(|err| {
                let msg = format!("failed to list messages: {}", err);
                error!(msg);
                Error::APIError { msg }
            })?;
        trace!("{:#?}", response);
        messages.append(&mut response.data);
        if !response.has_more {
            break;
        } else if let Some(message) = messages.last() {
            parameters.after = Some(message.id.clone());
        } else {
            // This should be unreachable because there should have been data
            // in the response and that was appended to the array.
            break;
        }
    }
    Ok(messages)
}

/// Create a run on a thread using an assistant
///
/// See: <https://platform.openai.com/docs/api-reference/runs/createRun>
pub async fn run_create(
    client: &Client,
    thread_id: &str,
    assistant_id: &str,
) -> Result<Run, Error> {
    let parameters = CreateRunParameters {
        assistant_id: assistant_id.to_string(),
        ..Default::default()
    };
    let run = client
        .assistants()
        .runs()
        .create(thread_id, parameters)
        .await
        .map_err(|err| {
            let msg = format!("failed to create run: {}", err);
            error!("{}", msg);
            Error::APIError { msg }
        })?;
    trace!("{:#?}", run);
    Ok(run)
}

/// Get a run on a thread
///
/// See: <https://platform.openai.com/docs/api-reference/runs/getRun>
pub async fn run_get(client: &Client, thread_id: &str, run_id: &str) -> Result<Run, Error> {
    let run = client
        .assistants()
        .runs()
        .retrieve(thread_id, run_id)
        .await
        .map_err(|err| {
            let msg = format!("failed to get run: {}", err);
            error!("{}", msg);
            Error::APIError { msg }
        })?;
    trace!("{:#?}", run);
    Ok(run)
}

/// Create a thread
///
/// See: <https://platform.openai.com/docs/api-reference/threads/createThread>
pub async fn thread_create(client: &Client) -> Result<Thread, Error> {
    let parameters = CreateThreadParameters {
        messages: Some(vec![]),
        // messages: Some(vec![ThreadMessage {
        //     role: ThreadMessageRole::User,
        //     content: "None".to_string(),
        //     file_ids: None,
        //     metadata: None,
        // }]),
        ..Default::default()
    };

    let thread = client
        .assistants()
        .threads()
        .create(parameters)
        .await
        .map_err(|err| {
            let msg = format!("failed to create thread: {}", err);
            error!("{}", msg);
            Error::APIError { msg }
        })?;
    trace!("{:#?}", thread);
    Ok(thread)
}

/// Delete a thread
///
/// See: <https://platform.openai.com/docs/api-reference/threads/deleteThread>
pub async fn thread_delete(client: &Client, thread_id: &str) -> Result<DeletedObject, Error> {
    let result = client
        .assistants()
        .threads()
        .delete(thread_id)
        .await
        .map_err(|err| {
            let msg = format!("failed to delete assistant: {}", err);
            error!(msg);
            Error::APIError { msg }
        })?;
    trace!("{:#?}", result);
    Ok(result)
}

/// Get a thread
///
/// See: <https://platform.openai.com/docs/api-reference/threads/getThread>
pub async fn thread_get(client: &Client, thread_id: &str) -> Result<Thread, Error> {
    let result = client
        .assistants()
        .threads()
        .retrieve(thread_id)
        .await
        .map_err(|err| {
            let msg = format!("failed to get thread: {}", err);
            error!(msg);
            Error::APIError { msg }
        })?;
    trace!("{:#?}", result);
    Ok(result)
}
